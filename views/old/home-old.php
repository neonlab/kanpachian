<body class="Site"> <!--These two classes are needed for the footer to stick to the bottom of the page-->
<main class="Site-content">

<div id="additional-info" style="padding-top:5px;height:70px">
    <div class="row">
        <div class="large-12 columns">
            <h2 class="color-white headings text-center">Go To Event</h2>
        </div>
    </div>
</div>

<div id="intro">
    <div class="row" style="text-align:center">
        <div class="wrapper" style="border:none;display:inline-block">
            <form action="<?=FRONT_ROOT?>Purchase/searchByArtist" method="post">
                <div class="input-group-rounded" style="display:inline-block">
                    <div style="float:left">
                        <input style="min-width:200px" class="animated-search-form input-group-field" name="q" type="search" placeholder="Busque por Artista..">
                    </div> 
                    <div class="input-group-button" style="float:left;height:37px">
                        <input type="submit" class="button secondary" value="Buscar">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
    <div class="wrapper" style="border:none">
        <?php
        $columnQuantity = 3;
        $boostrapDivision = 12/$columnQuantity;
        $i=0;
        //var_dump($eventList);
        foreach($eventList as $event) 
        {
            if($i==0){
        ?>
            <div class="grid-x grid-padding-x">
        <?php
            }
        ?>
        
        <div style="border:solid black" class="large-<?=$boostrapDivision?> medium-6 cell">
        <a id="a<?=$i?>" href=""><img width="200px" id="<?=$event->getIdEvent()?>" onclick="doSomething(this.id)" src="<?=IMG_PATH.$event->getImage()?>"></a>
        <div><?=$event->getEventName()?></div>
        <form action="<?=FRONT_ROOT?>Purchase/index" method="post">
        <input type="hidden" name="idEvent" value="<?=$event->getIdEvent()?>">
        <input  type="submit" class="button" value="Ver" id="<?=$event->getIdEvent()?>">
        </div>
        
        </form>
        <?php      
            if($i==$columnQuantity){
        ?>
                </div>  
                
        <?php
                $i=$columnQuantity;
            }
            $i++;
        }
        ?> 
    </div>
    </div>
</div>

<div id="features">
    
</div>

<?php require VIEWS_PATH."FooterUserView.php";?>

<style>
</style>